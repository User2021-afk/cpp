#include <iostream>

 int add(int x, int y); // это нужно для того, чтобы main.cpp знал, что функция add() определена в другом месте

 int main() {
std::cout << "The sum of 3 and 4 is: " << add(3, 4) << std::endl;
return 0;
 }
